package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.ITaskRepository;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;

import java.util.*;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final Comparator<Task> comparator) {
        return list.stream()
                .filter(t -> {
                    assert t.getUserId() != null;
                    return t.getUserId().equals(userId);
                })
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findById(userId, id) != null;
    }

    @NotNull
    @Override
    public Task findByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId).stream()
                .filter(t -> t.getName().equals(name))
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task findByIndex(@NotNull final String userId, @NotNull int index) {
        @Nullable
        final List<Task> tasks = findAll(userId);
        return tasks.get(index);
    }

    @NotNull
    @Override
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull
        final Optional<Task> task =  Optional.ofNullable(findByName(userId, name));
        task.ifPresent(this::remove);
        return task.orElseThrow(ProcessException::new);
    }

    @NotNull
    @Override
    public Task removeByIndex(@NotNull final String userId, @NotNull final int index) {
        @NotNull
        final Optional<Task> task =  Optional.ofNullable(findByIndex(userId, index));
        task.ifPresent(this::remove);
        return task.orElseThrow(ProcessException::new);
    }

    @Nullable
    @Override
    public Task startById(@NotNull final String userId, @NotNull final String id) {
        @Nullable
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task startByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task startByIndex(@NotNull final String userId, @NotNull final int index) {
        @Nullable
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task finishById(@NotNull final String userId, @NotNull final String id) {
        @Nullable
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task finishByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task finishByIndex(@NotNull final String userId, @NotNull final int index) {
        @Nullable
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @Nullable
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        @Nullable
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusByIndex(@NotNull final String userId, @NotNull final int index, @NotNull final Status status) {
        @Nullable
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @NotNull final String id) {
        return findAll(userId).stream()
                .filter(t -> t.getUserId().equals(userId))
                .filter(t -> t.getProjectId().equals(id))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Task bindTaskToProjectById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        @NotNull
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        task.setUserId(userId);
        return task;
    }

    @NotNull
    @Override
    public Task unbindTaskById(@NotNull final String userId, @NotNull final String id) {
        @NotNull
        final Task task = findById(userId, id);
        task.setUserId(null);
        task.setProjectId(null);
        return task;
    }

    @NotNull
    @Override
    public void unbindAllTaskByProjectId(@NotNull final String userId, @NotNull final String id) {
        findAll(userId).stream()
                .filter(t -> t.getProjectId().equals(id))
                .filter(t -> t.getUserId().equals(userId))
                .forEach(t -> t.setProjectId(null));
    }

}