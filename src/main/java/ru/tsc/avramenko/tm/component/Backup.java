package ru.tsc.avramenko.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.avramenko.tm.api.service.IPropertyService;
import ru.tsc.avramenko.tm.command.data.BackupLoadCommand;
import ru.tsc.avramenko.tm.command.data.BackupSaveCommand;

public class Backup extends Thread {

    @NotNull
    final Bootstrap bootstrap;
    private final int INTERVAL;

    public Backup(@NotNull final Bootstrap bootstrap, @NotNull final IPropertyService propertyService) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
        this.INTERVAL = propertyService.getBackupInterval();
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(INTERVAL);
        }
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        bootstrap.parseCommand(BackupSaveCommand.BACKUP_SAVE);
    }

    public void load() {
        bootstrap.parseCommand(BackupLoadCommand.BACKUP_LOAD);
    }

}