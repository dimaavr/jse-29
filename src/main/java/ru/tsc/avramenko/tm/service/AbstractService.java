package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.api.IService;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.model.AbstractEntity;

import java.util.List;

public abstract class AbstractService <E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void add(@Nullable final E entity) {
        if (entity == null) return;
        repository.add(entity);
    }

    @Override
    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public E findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @NotNull
    @Override
    public E removeById(@Nullable final String id) {
        if (id == null) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public void addAll(@Nullable final List<E> entities) {
        if (entities == null) return;
        repository.addAll(entities);
    }


}